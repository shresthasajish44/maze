using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public float moveSpeed = 100;
    public BallInputManager ballController;
    //public InputAction ballControls;
    public Vector2 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        ballController = new BallInputManager();
    }

    public void Jump() {
        Debug.Log(transform.position);
        transform.Translate(Vector3.up * 150f * Time.deltaTime) ;
    }

    public void OnEnable()
    {
        ballController.Enable();
    }

    public void OnDisable()
    {
        ballController.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        moveDirection = ballController.BallMovements.Move.ReadValue<Vector2>();
        Debug.Log(moveDirection);
        
        if (ballController.BallMovements.Move.IsPressed()) {
            Debug.Log("is pressed");
            transform.Translate(new Vector3(moveDirection.y, 0f, -moveDirection.x) * moveSpeed * Time.deltaTime);
        }

    }
}
